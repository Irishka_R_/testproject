import {Router, Request, Response} from 'express';
import mongoose = require("mongoose");
import CardController from '../controllers/card';

class CardRouter {

    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     *
     * @param req
     * @param res
     * @constructor
     */
    public GetCard(req: Request, res: Response): void {

        const status = req.statusCode ? req.statusCode : 200;
        const id: number = parseInt(req.params.id) ? parseInt(req.params.id) : 9;
        CardController
            .getCard(id)
            .then((data) => {
                res.json({
                    status,
                    data
                });
            })
            .catch((err) => {
                res.json({
                    status,
                    err: err.message
                })
            });
    };

    routes() {
        this.router.get('/:id', this.GetCard);
        this.router.get('/', this.GetCard);
    }

}

const cardRouter = new CardRouter();
cardRouter.routes();

export default cardRouter.router;

