import {Router, Request, Response} from 'express';
import * as EmailValidator from 'email-validator';
import CustomerController from '../controllers/customer';
import CardController from '../controllers/card';
import mongoose = require("mongoose");
import RuleEngine = require('node-rules');
let bcrypt = require('bcrypt-nodejs');

class AnswerRouter {

    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     *
     * @param req
     * @param res
     * @constructor
     */
    public CreateCustomer(req: Request, res: Response): void {

        const status = req.statusCode ? req.statusCode : 200;
        let firstCard: any = null;
        if (req.body) {
            const card_number: number = req.body.card_number ? req.body.card_number : 9;
            CardController
                .firstCard(card_number)
                .then((first) => {
                    firstCard = first;
                    if (first) {
                        if (!req.body.email)
                            throw new Error('Email required');
                        if (!req.body.password)
                            throw new Error('Pasword required');
                        if (!EmailValidator.validate(req.body.email))
                            throw new Error('No valid email');
                        const email: string = req.body.email;
                        const password: string = bcrypt.hashSync(req.body.password);
                        const by_email: boolean = req.params.by_email ? req.params.by_email : false;
                        return CustomerController.createCustomer(email, password, by_email, card_number);
                    } else {
                        return CustomerController.getCustomer(req.body);
                    }
                })
                .then((data: any) => {
                    res.json({
                        status,
                        data: data.card,
                        customer_id: data.customer_id
                    });
                }).catch((error) => {
                res.json({
                    status,
                    err: error.message
                });

            });
        }

    }

    routes() {
        this.router.post('/', this.CreateCustomer);
    }

}

const answerRouter = new AnswerRouter();
answerRouter.routes();

export default answerRouter.router;





