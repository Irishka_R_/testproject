import {suite, test} from "mocha-typescript";
import Config from "../config";
import mongoose = require("mongoose");
import ActionController from "../controllers/action";

@suite
class TestAction {

    private _expectedData: any;

    public static before() {

        //use q promises
        global.Promise = require("q").Promise;
        //use q library for mongoose promise
        mongoose.Promise = global.Promise;
        //connect to mongoose and create model
        const MONGODB_CONNECTION: string = Config.MONGODB_CONNECTION;
        let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION);
        mongoose.connect(Config.MONGODB_CONNECTION);

        //require chai and use should() assertions
        let chai = require("chai");
        chai.should();
    }

    constructor() {
    }

    @test("should validate postcode")
    public testValidatePostcode() {
        let res = ActionController.validatePostalCode('ID1 1QD');
        res.should.equal(true);
    }

    @test("should validate telenum")
    public testValidatePhoneNumber() {
        let res = ActionController.validatePhoneNumber('+18582950614');
        res.should.equal(true);
    }


}