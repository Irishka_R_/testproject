import {suite, test} from "mocha-typescript";
import Config from "../config";
import mongoose = require("mongoose");
import CardController from "../controllers/card";

@suite
class TestCard {

    private _expectedData: any;
    private _expectedNextCard: string;

    public static before() {

        //use q promises
        global.Promise = require("q").Promise;
        //use q library for mongoose promise
        mongoose.Promise = global.Promise;
        //connect to mongoose and create model
        const MONGODB_CONNECTION: string = Config.MONGODB_CONNECTION;
        let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION);
        mongoose.connect(Config.MONGODB_CONNECTION);

        //require chai and use should() assertions
        let chai = require("chai");
        chai.should();
    }

    constructor() {
        this._expectedData = {
            location_order: 'B001',
            question: 'Create a secure account and start your mortgage journey',
            outcome_type: [{type: 'Text', value: 'email'},
                {type: 'Text', value: 'password'}],
            card_number: 9,
            positive_res: 677,
            negative_res: 9,
            next_card: 677
        };
        this._expectedNextCard = 'B003';
    }

    @test("should get a card")
    public testGetCard() {

        return CardController.getCard(9).then((result: any) => {
            result.location_order.should.equal(this._expectedData.location_order);
        });
    }

    @test("should get next a card")
    public testGetNextCard() {

        return CardController.nextCard('B002').then((result: any) => {
            result.location_order.should.equal(this._expectedNextCard);
        });

    }

    @test("check first card or not")
    public testFirstCard() {

        return CardController.firstCard(9).then((result: any) => {
            result.should.equal(true);
        });

    }

    @test("check firs card. negative res")
    public testNegativeRes() {
        return CardController.firstCard(644).then((result: any) => {
            result.should.equal(false);
        });

    }


}