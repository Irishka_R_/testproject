import mongoose = require("mongoose");
import Customer from '../models/customer';
import ActionController from '../controllers/action';
import CardController from '../controllers/card';
let bcrypt = require('bcrypt-nodejs');
let Promise = require('es6-promise').Promise;

class CustomerController {
    private static instance: CustomerController;

    constructor() {
        if (CustomerController.instance) {
            throw new Error("Error - use Singleton.getInstance()");
        }
        this.member = 0;
    }

    static getInstance(): CustomerController {
        CustomerController.instance = CustomerController.instance || new CustomerController();
        return CustomerController.instance;
    }

    member: number;

    /**
     *
     * @param email
     * @param password
     * @param by_email
     * @param card_number
     * @returns {Promise<TRes>}
     */
    public createCustomer(email: string, password: string, by_email: boolean, card_number: number) {
        return Customer.create({
            email: email,
            password: password,
            by_email: by_email,
            card_number: card_number
        })
            .then((data) => {
                if(!data)
                    throw new Error("Customer not saved");
                return CardController
                    .nextCard('')
                    .then((card) => {
                        return {
                            card: card,
                            customer_id: data._id
                        }
                    })
            })
    }

    /**
     *
     * @param data
     * @returns {Promise<TRes>|PromiseLike<Promise<TRes>>}
     */
    public getCustomer(data: any) {

        const id: string = data.customer_id;
        const card_number: number = data.card_number;

        if (!id)
            throw new Error("Customer ID required");
        if (!card_number)
            throw new Error("Card number required");
        return CardController
            .getCardRules(card_number).then((card) => {
                if (!card)
                    throw new Error("Card not found");
                return this.checkCustomer(data, card)
            })

    }

    /**
     *
     * @param res
     * @param data
     * @param code
     * @param age
     * @returns {Promise<any>|void}
     */
    public updateCustomer(res: any, data: any, code: string, age: number, addresses: any, location_order: string, next_card: number, address: string) {

        Object.keys(data).map((item) => {
            if (item != 'customer_id')
                res[item] = data[item];
        });
        if (code)
            res.code = code;
        if (age)
            res.age = age;
        if (address)
            res.address = mongoose.Types.ObjectId(address);
        let outcome_type: any;
        let savePromise = new Promise((resolve) => {
            res.save((err, data) => {
                if (err)
                    throw new Error("Customer not saved. Please try again");
                let cardPromise = next_card ? CardController.getCard(next_card) : CardController.nextCard(location_order);
                cardPromise.then((card) => {
                    outcome_type = card.outcome_type.forEach((item) => {
                        if (res[item.key]) {
                            item.value = res[item.key]
                        }
                        if (addresses && item.key == 'address') {
                            item.value = addresses;
                            if (item.params) {
                                item.value.push('none of these');
                            }
                        }
                    });
                    resolve({card: card, customer_id: res._id});
                })

            });
        })
        return savePromise;
    }

    /**
     *
     * @param data
     * @param card
     * @returns {Promise<TRes>}
     */
    public checkCustomer(data: any, card: any) {
        const id: string = data.customer_id;
        if (!id)
            throw new Error("Customer ID required");
        if (!card)
            throw new Error("Card required");
        return Customer
            .findOne({
                _id: id
            })
            .then((res) => {
                return card.rules ? ActionController.checkRules(data, res, card) : this.updateCustomer(res, data, null, null, null, card.card_number, null, null);
            })
    }

}

export default CustomerController.getInstance();