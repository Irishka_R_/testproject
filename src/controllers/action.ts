import mongoose = require("mongoose");
import AddressController from '../controllers/address';
import CustomerController from '../controllers/customer';
let RuleEngine = require('node-rules');
import Config from "../config";
let Promise = require('es6-promise').Promise;
let client = require('twilio')(Config.TWILIO_ACCOUNT_SID, Config.TWILIO_AUTH_TOKEN);
let idealPostcodes = require("ideal-postcodes")(Config.POSTCODE_KEY);

class ActionController {

    private static instance: ActionController;

    constructor() {
        if (ActionController.instance) {
            throw new Error("Error - use Singleton.getInstance()");
        }
        this.member = 0;
    }

    static getInstance(): ActionController {
        ActionController.instance = ActionController.instance || new ActionController();
        return ActionController.instance;
    }

    member: number;

    /**
     *
     * @returns {number}
     */
    public generateUniqueCode() {
        return Math.floor(Math.pow(10, 5) + Math.random() * (Math.pow(10, 6) - Math.pow(10, 5) - 1));
    }

    /**
     *
     * @param code
     * @param telenum
     * @returns {Promise<T>|HTMLImageElement|LoDashImplicitObjectWrapper<T&{from: string, to: string, body: string}>|XMLHttpRequest|MongoError|any}
     */
    public sendSms(code: number, telenum: string) {
        return client.messages.create({
            from: Config.PHONE_NUMBER,
            to: telenum,
            body: Config.SMS_MESSAGE + code
        }, (err) => {
            if (err)
                throw new Error(err.message);
            return true
        });
    }

    /**
     *
     * @param postcode
     * @returns {boolean}
     */
    public validatePostalCode(postcode: string) {
        if (!postcode)
            throw new Error('Postcode required')
        let regExp = /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$/;
        return regExp.test(postcode) ? true : false
    }

    /**
     *
     * @param telenum
     * @returns {boolean}
     */
    public validatePhoneNumber(telenum: string) {
        if (!telenum)
            throw new Error('Telenum required')
        let patt = new RegExp(/\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?/);
        return patt.test(telenum)
    }

    /**
     *
     * @param dob
     * @returns {number}
     */
    public getAge(dob: string) {
        if (!dob)
            throw new Error('DoB required')
        let birthday = +new Date(dob);
        return (Date.now() - birthday) / (31557600000);
    }

    /**
     *
     * @param data
     */
    public cresteNewAddress(data: any) {
        return AddressController.addAdress(data).then((address) => {
            return address
        });
    }

    /**
     *
     * @param postcode
     * @returns {Promise<TRes>|PromiseLike<TResult>}
     */
    public getAddress(postcode: string) {
        return AddressController.findAdresses(postcode).then((data) => {
            if (data && !data.length) {
                let addressPromise = new Promise((resolve) => {
                    idealPostcodes.lookupPostcode(postcode, (error, addresses) => {
                        if (error && !addresses)
                            return {}
                        let res = [];
                        addresses.map((value) => {
                            res.push({
                                line_one: value.line_1,
                                line_two: value.line_2,
                                postcode: value.postcode,
                                city: value.post_town
                            });
                            return res
                        });
                        AddressController.addAdresses(res).then((address) => {
                            resolve(address)
                        });
                    });
                });
                return addressPromise
            } else {
                return data
            }
        })

    }

    /**
     *
     * @param data
     * @param customer
     * @param card
     * @returns {void|Promise<BulkWriteResult>}
     */
    public checkRules(data: any, customer: any, card: any) {

        let code: string = customer.code ? customer.code : '';
        let age: number = customer.age ? customer.age : '';
        let postcode: number = customer.postcode ? customer.postcode : '';
        let addresses: any;
        let next_card: number;
        let address: string;

        if (card) {

            let rules = [{
                "condition": eval('(' + card.rules.condition + ')'),
                "consequence": eval('(' + card.rules.consequence + ')')
            }];

            Object.keys(card.rules.facts).map((item) => {
                if (item.indexOf('action') !== -1)
                    card.rules.facts[item] = eval('(' + card.rules.facts[item] + ')');
                else
                    card.rules.facts[item] = data[item];
            });

            let fact = card.rules.facts;

            let R = new RuleEngine(rules);
            let factPromise = new Promise((resolve) => {
                R.execute(fact, (res) => {
                    if (!res.result)
                        next_card = card.rules.negative_res ? card.rules.negative_res : card.card_number;
                    if (card.rules.positive_res)
                        next_card = card.rules.positive_res;
                    if (res.result && !addresses && next_card)
                        next_card = next_card;
                    CustomerController.updateCustomer(customer, data, code, age, addresses, card.location_order, next_card, address).then((data) => {
                        resolve(data);
                    });
                })
            });
            return factPromise;

        }
    }

}

export default ActionController.getInstance();
