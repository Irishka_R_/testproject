import mongoose = require("mongoose");
import Card from '../models/card';
import RuleEngine = require('node-rules');

class CardController {
    private static instance: CardController;

    constructor() {
        if (CardController.instance) {
            throw new Error("Error - use Singleton.getInstance()");
        }
        this.member = 0;
    }

    static getInstance(): CardController {
        CardController.instance = CardController.instance || new CardController();
        return CardController.instance;
    }

    member: number;

    /**
     *
     * @param card_number
     * @returns {any}
     */
    public firstCard(card_number: number) {
        return Card.find({})
            .sort({'location_order': 1})
            .limit(1)
            .then((data) => {
                let result = this.toObject(data);
                if (result && result.card_number == card_number)
                    return true;
                else
                    return false;
            })
    }

    /**
     *
     * @param id
     * @returns {Promise<TRes>|PromiseLike<TResult>}
     */
    public getCard(id: number) {
        let result: any;
        return Card.aggregate([
            {$match: {"card_number": id}},
            {
                $project: {
                    _id: 0, question: 1, outcome_type: 1, card_number: 1, positive_res: 1, negative_res: 1,location_order:1
                }
            }])
            .exec()
            .then((data) => {
                if (!data || !data.length)
                    throw new Error("No cards found");
                result = this.toObject(data);
                return this.nextCard(result.location_order);
            }).then((res) => {
                if (res)
                    result.next_card = res.card_number;
                return result

            });
    }

    /**
     *
     * @param id
     * @returns {Promise<TRes>}
     */
    public getCardRules(id: number) {
        return Card.aggregate([
            {$match: {"card_number": id}},
            {
                $lookup: {
                    from: "rules",
                    localField: "rules",
                    foreignField: "_id",
                    as: "rules"
                }
            },
            {$unwind: {path: "$rules", preserveNullAndEmptyArrays: true}},
            {
                $project: {
                    _id: 0, question: 1, outcome_type: 1, card_number: 1, rules: 1,location_order:1
                }
            }])
            .exec()
            .then((data) => {
                return data ? this.toObject(data): data
            });
    }

    /**
     *
     * @param card_number
     * @returns {Promise<TRes>}
     */
    public nextCard(location_order: string) {
        return Card
            .aggregate([
                {$match: {location_order: {$gt: location_order}}},
                {$sort: {location_order: 1}},
                {$limit: 1},
                { $project: {
                    _id: 0, question: 1, outcome_type: 1, card_number: 1, positive_res: 1, negative_res: 1,location_order:1      }}
                ])
            .then((data) => {
                return data ? this.toObject(data): data
            });
    }

    /**
     *
     * @param data
     * @returns {any}
     */
    private toObject(data: any) {
        if (data && data.length)
            return data[0]
        else
            return {}
    }
}

export default CardController.getInstance();