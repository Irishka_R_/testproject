import mongoose = require("mongoose");
import Address from '../models/address';

class AddressController {
    private static instance: AddressController;

    constructor() {
        if (AddressController.instance) {
            throw new Error("Error - use Singleton.getInstance()");
        }
        this.member = 0;
    }

    static getInstance(): AddressController {
        AddressController.instance = AddressController.instance || new AddressController();
        return AddressController.instance;
    }

    member: number;

    /**
     *
     * @param data
     * @returns {Promise<TRes>}
     */
    public addAdresses(data: any) {
        return Address.insertMany(data)
            .then((data) => {
                return data
            });
    }

    /**
     *
     * @param postcode
     * @returns {any}
     */
    public findAdresses(postcode: string) {
        return Address
            .find({postcode: postcode})
            .then((data) => {
                return data
            });
    }

    /**
     *
     * @param data
     */
    public addAdress(data: any) {
        return Address.create(data)
            .then((res) => {
                if (!res)
                    throw new Error('Address not create');
                return res
            });
    }


}

export default AddressController.getInstance();
