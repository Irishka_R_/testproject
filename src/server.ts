import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as helmet from "helmet";
let validator = require('validator');


import * as path from "path";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");
import mongoose = require("mongoose");


//routes
import CardRouter from "./routes/card";
import Config from "./config";
import AnswerRouter from "./routes/customer";

/**
 * The server.
 *
 * @class Server
 */
export class Server {

    public app: express.Application;

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {

        this.app = express()

        this.config();

        this.routes();
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {

        //mount logger
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.app.use(bodyParser.json());
        this.app.use(helmet());
        this.app.use(logger("dev"));

        this.app.use(cookieParser("SECRET_GOES_HERE"));

        this.app.use(methodOverride());

        //use q promises
        global.Promise = require("q").Promise;
        mongoose.Promise = global.Promise;

        let connection: mongoose.Connection = mongoose.createConnection(Config.MONGODB_CONNECTION);
        connection.on('connected', function () {
            console.log("Connected to mongodb successfully");
        });
        mongoose.connect(Config.MONGODB_CONNECTION);

        // catch 404 and forward to error handler
        this.app.use(function (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });

        //error handling
        this.app.use(errorHandler());
    }

    /**
     * Create and return Router.
     *
     * @class Server
     * @method config
     * @return void
     */
    private routes(): void {
        let router: express.Router = express.Router();

        this.app.use('/card', CardRouter);
        this.app.use('/answer', AnswerRouter);
        this.app.use(router);
    }

}

export default new Server().app;