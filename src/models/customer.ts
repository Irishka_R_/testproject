import {model, Schema} from "mongoose";

export let customerSchema: Schema = new Schema({
    createdAt: Date,
    by_email: Boolean,
    email: {type: String, required: true},
    password: {type: String, required: true},
    telenum: String,
    code: Number,
    title: String,
    fist_name: String,
    surname: String,
    dob: Date,
    age: Number,
    postcode: String,
    card_number: Number,
    resend: Boolean,
    contact_you_as: String


});

customerSchema.pre("save", function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

export default model('Customer', customerSchema)
