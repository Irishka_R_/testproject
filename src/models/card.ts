import {Schema, model} from "mongoose";

export let cardSchema: Schema = new Schema({
    location_order: String,
    question: {type: String, required: true},
    outcome_type: [{
        type: {type: String, required: true},
        value: {type: Array},
        params: {type: String},
        key: {type: String}
    }],
    card_number: Number,
    rules: {type: Schema.Types.ObjectId, ref: 'Rule'}
});

export default model('Card', cardSchema)