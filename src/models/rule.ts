import {Schema, model} from "mongoose";

export let ruleSchema: Schema = new Schema({
    priority: {type: Number, min: 0, max: 10, required: true},
    condition: {type: String, required: true},
    consequence: {type: String, required: true},
    facts: {type: String},
    positive_res: Number,
    negative_res: Number,
});

export default model('Rule', ruleSchema)