import {Schema, model} from "mongoose";

export let addressSchema: Schema = new Schema({
    line_one: String,
    line_two: String,
    postcode: {type: String, required: true},
    city: String

});

export default model('Address', addressSchema)