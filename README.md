# Test Project

Test project. Server is written with NodeJs + TypeScript+ MongoDB(Mongoose)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Latest node version
Install nodemon
Install mocha global
Install npm, if not installed

### Installing

To install necessary to run:

$ npm install

### Building the project

First you need to compile ts code to JS

$ grunt
OR
$ npm run grunt

### Running the project

$ nodemon ./bin/www
OR
$ npm run start


## Running tests

$ mocha dist/test
OR
$ npm run test

###Additional information

DB dump is in db_cards.zip
Guideline is available here: https://docs.mongodb.com/manual/reference/program/mongorestore/

I've used the following plug-in for rules: https://github.com/mithunsatheesh/node-rules

###GET first card

url = http://localhost:8000/card

###POST requests samples

1) url = http://localhost:8000/answer
body ={
	"card_number": 9,
	"email":"test@test.com",
	"password":"1234567"
}

2) url = http://localhost:8000/answer
body = {
	"card_number": 677,
	"customer_id":"598c2ba8109d468ce457ad63",
	"telenum":"+18582950614"
}

3) url = http://localhost:8000/answer
body =
{
	"card_number": 46,
	"code": 385371,
	"customer_id":"598c2ba8109d468ce457ad63",
	"telenum":"+18582950614"
}

4) url = http://localhost:8000/answer
body =
{
	"card_number": 735,
	"resend":true,
	"customer_id":"598c2ba8109d468ce457ad63",
	"telenum":"+18582950614"
}
